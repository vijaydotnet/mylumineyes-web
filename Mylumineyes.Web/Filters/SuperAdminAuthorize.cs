﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mylumineyes.Filters
{
    public class SuperAdminAuthorize : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            RouteData routeData = filterContext.RouteData;
            if (HttpContext.Current.User.Identity.Name == "")
            {
                HandleUnauthorizedRequest(filterContext);
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            string URL = ((System.Web.Routing.Route)(filterContext.RequestContext.RouteData.Route)).Url;
            if (!URL.StartsWith("/"))
            {
                URL = "/" + URL;
            }
            filterContext.Result = new RedirectToRouteResult(new
            RouteValueDictionary(new { controller = "Account", action = "Login", area = "" }));
        }
    }
}