﻿using System.Web;
using System.Web.Optimization;

namespace Mylumineyes.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/bundlecss").Include(
                    "~/Content1/css/bootstrap.min.css",
                    "~/Content1/css/bootstrap-extend.css",
                    "~/Content1/css/master_style.css",
                    "~/Content1/css/custom.css",
                    "~/Content1/css/skin-orange.css",
                    "~/Content1/css/site.css").Include("~/Content/font-awesome.min.css", new CssRewriteUrlTransform()));
            bundles.Add(new StyleBundle("~/Content/SuperAdmin/bundlecss").Include(
                 "~/Areas/SuperAdmin/Content/css/bootstrap.min.css",
                 "~/Areas/SuperAdmin/Content/css/bootstrap-extend.css",
                 "~/Areas/SuperAdmin/Content/css/master_style.css",
                 "~/Areas/SuperAdmin/Content/css/skin-orange.css",
                 "~/Areas/SuperAdmin/Content/css/custom.css",
                  "~/Areas/SuperAdmin/Content/css/bootstrap-datepicker.min.css",
                  "~/Areas/SuperAdmin/Content/css/morris.css",
                  "~/Areas/SuperAdmin/Content/css/select2.min.css",
                 "~/Areas/SuperAdmin/Content/css/sweetalert.css"));

            bundles.Add(new ScriptBundle("~/bundles/SuperAdmin/jquery").Include(
                   "~/Areas/SuperAdmin/Content/js/jquery-1.10.2.min.js",
                       "~/Areas/SuperAdmin/Content/js/jquery.js",
                       "~/Areas/SuperAdmin/Content/js/jquery-ui.js",
                       "~/Areas/SuperAdmin/Content/js/popper.min.js",
                       "~/Areas/SuperAdmin/Content/js/bootstrap.min.js",
                       "~/Areas/SuperAdmin/Content/js/raphael.min.js",
                       "~/Areas/SuperAdmin/Content/js/morris.min.js",
                       "~/Areas/SuperAdmin/Content/js/chart.js",
                       "~/Areas/SuperAdmin/Content/js/jquery.sparkline.js",
                       "~/Areas/SuperAdmin/Content/js/jquery.slimscroll.js",
                         "~/Areas/SuperAdmin/Content/js/fastclick.js",
                       "~/Areas/SuperAdmin/Content/js/template.js",

                        "~/Areas/SuperAdmin/Content/js/jquery.dataTables.min.js",
                       "~/Areas/SuperAdmin/Content/js/dataTables.bootstrap.min.js",

                       "~/Areas/SuperAdmin/Content/js/jquery.dataTables.min.js",
                       "~/Areas/SuperAdmin/Content/js/sweetalert.min.js",
                       "~/Areas/SuperAdmin/Content/js/jquery.validate.js",
                       "~/Areas/SuperAdmin/Content/js/jquery.validate.unobtrusive.js",
                       "~/Areas/SuperAdmin/Content/js/bootstrap-datepicker.min.js",
                       "~/Areas/SuperAdmin/Content/js/dataTables.buttons.min.js",
                       "~/Areas/SuperAdmin/Content/js/jquery.sweet-alert.custom.js",
                       "~/Areas/SuperAdmin/Content/js/buttons.flash.min.js",
                       "~/Areas/SuperAdmin/Content/js/jszip.min.js",
                       "~/Areas/SuperAdmin/Content/js/pdfmake.min.js",
                       "~/Areas/SuperAdmin/Content/js/vfs_fonts.js",
                       "~/Areas/SuperAdmin/Content/js/buttons.html5.min.js",
                       "~/Areas/SuperAdmin/Content/js/buttons.print.min.js",
                       "~/Areas/SuperAdmin/Content/js/data-table.js"

                       ));
        }
    }
}
