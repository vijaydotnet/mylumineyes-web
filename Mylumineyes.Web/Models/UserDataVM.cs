﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mylumineyes.Web.Models
{
    public class UserDataVM
    {
        public string FullName { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string Obeservations { get; set; }
        public string File1 { get; set; }
        public string File2 { get; set; }
        public Guid Id { get; set; }
        public DateTime CreatedDate{get;set;}
    }
}