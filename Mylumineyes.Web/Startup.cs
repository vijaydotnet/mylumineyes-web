﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mylumineyes.Startup))]
namespace Mylumineyes
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
