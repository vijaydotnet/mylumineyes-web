﻿using Mylumineyes.Web.database;
using Mylumineyes.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Mylumineyes.Web.Controllers.api
{
    //string fullname, phone, email, obeservations, file1 file2
    [RoutePrefix("api")]
    public class UserDataController : ApiController
    {
        // GET: api/ManageData


        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/ManageData/5
        [Route]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/ManageData

        //[Route("saveData")]

        [HttpPost]
        [Route("uploadImages")]
        public IHttpActionResult UploadImages()
        {
            // UserDataVM model = new UserDataVM();
            ResponseVM response = new ResponseVM
            {
                Status = false,
                StatusCode = HTTPStatusCode.BadRequest,
                Message = "Bad request"
            };
           
            string filePath1 = "";
            string filePath2 = "";
            try
            {

                string fileName = string.Empty;
                var httpRequest = HttpContext.Current.Request;

                if (httpRequest.Files.Count > 1)
                {
                    var rootFolder = ConfigurationManager.AppSettings["FilePath"];
                    var rootPath = HttpContext.Current.Server.MapPath("~" + rootFolder);
                    //generate folder if not exists
                    if (!Directory.Exists(rootPath))
                    {
                        Directory.CreateDirectory(rootPath);
                    }
                    //save files and generate unique name
                    var file1 = "";
                    var file2 = "";

                    var postedFile1 = httpRequest.Files[0];
                    var postedFile2 = httpRequest.Files[1];

                    //upload file 1
                    string ext = Path.GetExtension(postedFile1.FileName);
                    ext = ext.ToLower();
                    string uniqueId = Guid.NewGuid().ToString() + ext;
                    file1 = rootPath + "" + uniqueId;

                    filePath1 = rootFolder + uniqueId;

                    postedFile1.SaveAs(file1);


                    //upload file 2
                    ext = Path.GetExtension(postedFile2.FileName);
                    ext = ext.ToLower();
                    uniqueId = Guid.NewGuid().ToString() + ext;
                    file2 = rootPath + "" + uniqueId;

                    filePath2 = rootFolder + uniqueId;

                    postedFile2.SaveAs(file2);

                }
                else
                {
                    response.Message = "Missing attachment, Please capture left and right eyes.";
                }
                if (filePath1 != string.Empty && filePath2 != string.Empty)
                {
                    response.Status = true;
                    response.StatusCode = HTTPStatusCode.OK;
                    response.Message = "Images uploaded successfully.";
                    response.Data = new
                    {
                        File1 = filePath1,
                        File2 = filePath2
                    };
                }
                else
                {
                    //response.StatusCode = HTTPStatusCode.CatchException;
                    response.Message = "Failed, please try again";
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.InnerException != null ? ex.InnerException.InnerException != null ? ex.InnerException.InnerException.Message : ex.InnerException.Message : ex.Message;
                response.StatusCode = HTTPStatusCode.CatchException;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("GetData")]
        public IHttpActionResult GetData()
        {
            ResponseVM response = new ResponseVM
            {
                Status = false,
                StatusCode = HTTPStatusCode.BadRequest,
                Message = "Bad request"
            };
            try
            {
                dbMylumineyesEntities db = new dbMylumineyesEntities();
                var result = db.tblUserDatas.Select(x => new UserDataVM {
                    Email=x.Email,
                    File1=x.File1,
                    File2=x.File2,
                    FullName=x.FullName,
                    Obeservations=x.Obeservations,
                    PhoneNo=x.PhoneNo
                }).ToList();
                response.Data = result;
                response.Message = "Success";
                response.StatusCode = HTTPStatusCode.OK;
            }
            catch (Exception ex)
            {
                response.Message = ex.InnerException != null ? ex.InnerException.InnerException != null ? ex.InnerException.InnerException.Message : ex.InnerException.Message : ex.Message;
                response.StatusCode = HTTPStatusCode.CatchException;
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveData")]
        public IHttpActionResult SaveUserData(UserDataVM model)
        {
            // UserDataVM model = new UserDataVM();
            ResponseVM response = new ResponseVM
            {
                Status = false,
                StatusCode = HTTPStatusCode.BadRequest,
                Message = "Bad request"
            };
            try
            {
                dbMylumineyesEntities db = new dbMylumineyesEntities();
                var entity = new tblUserData();
                entity.ID = Guid.NewGuid();
                entity.CreateDate = DateTime.UtcNow;
                entity.Email = model.Email;
                entity.File1 = model.File1;
                entity.File2 = model.File2;
                entity.FullName = model.FullName;
                entity.Obeservations = model.Obeservations;
                entity.PhoneNo = model.PhoneNo;

                db.tblUserDatas.Add(entity);
                db.SaveChanges();
                response.Status = true;
                response.StatusCode = HTTPStatusCode.OK;
                response.Message = "Data posted successfully.";

            }
            catch (Exception ex)
            {
                response.Message = ex.InnerException != null ? ex.InnerException.InnerException != null ? ex.InnerException.InnerException.Message : ex.InnerException.Message : ex.Message;
                response.StatusCode = HTTPStatusCode.CatchException;
            }
            return Ok(response);
        }

        // PUT: api/ManageData/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ManageData/5
        public void Delete(int id)
        {
        }
    }
}
