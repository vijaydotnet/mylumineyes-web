﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Mylumineyes.Areas.SuperAdmin.Models;
using Mylumineyes.Filters;
using Mylumineyes.Web.database;
using Mylumineyes.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Mylumineyes.Web.Areas.SuperAdmin.Controllers
{
    //[SuperAdminAuthorize]
    public class DashboardController : Controller
    {
        // GET: SuperAdmin/Dashboard
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;
        private dbMylumineyesEntities dbContext = new dbMylumineyesEntities();
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        public ActionResult Download(Guid docId, string col)
        {
            try
            {
                var objDoc = dbContext.tblUserDatas.Where(x => x.ID == docId).FirstOrDefault();
                if (objDoc != null)
                {
                    var file = "";
                    if (col == "File1")
                    {
                        file = Server.MapPath("~" + objDoc.File1);
                    }
                    else
                    {
                        file = Server.MapPath("~" + objDoc.File2);
                    }
                    string ext = Path.GetExtension(file);
                    ext = ext.ToLower();
                    var fileName = Path.GetFileName(file);
                    var contentType = GetContentType(ext);
                    return File(file, contentType, fileName);
                }
            }
            catch (Exception ex)
            {
            }
            return View();
        }
        public string GetContentType(string ext)
        {
            string contentType = "";
            try
            {
                switch (ext)
                {
                    case ".jpg":
                        contentType = "image/jpg";
                        break;
                    case ".jpeg":
                        contentType = "image/jpeg";
                        break;
                    case ".pjpeg":
                        contentType = "image/pjpeg";
                        break;
                    case ".gif":
                        contentType = "image/gif";
                        break;
                    case ".x-png":
                        contentType = "image/x-png";
                        break;
                    case ".png":
                        contentType = "image/png";
                        break;
                }
            }
            catch (Exception ex)
            {
            }
            return contentType;
        }
        public ActionResult DeleteRequest(Guid id)
        {
            var Success = false;
            var Message = string.Empty;

            var entity = dbContext.tblUserDatas.Where(x => x.ID == id).FirstOrDefault();
            if (entity != null)
            {
                dbContext.tblUserDatas.Remove(entity);
                dbContext.SaveChanges();
                Success = true;
                Message = "Deleted Successfully";
            }
            else
            {
                Success = false;
                Message = "Record not found";
            }
            return Json(new
            {
                Success = Success,
                Message = Message
            }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SearchRequests(DateTime? dtFrom, DateTime? dtTo)
        {
            var UserId = User.Identity.GetUserId();
            List<UserDataVM> model = new List<UserDataVM>();
            try
            {
                model = dbContext.tblUserDatas.Where(x => (DbFunctions.TruncateTime(x.CreateDate) >= DbFunctions.TruncateTime(dtFrom.Value) || dtFrom == null) && (DbFunctions.TruncateTime(x.CreateDate) <= DbFunctions.TruncateTime(dtTo.Value) || dtTo == null)).OrderByDescending(x => x.CreateDate).Select(x => new UserDataVM
                {
                    Email = x.Email,
                    File1 = x.File1,
                    File2 = x.File2,
                    FullName = x.FullName,
                    Id = x.ID,
                    Obeservations = x.Obeservations,
                    PhoneNo = x.PhoneNo,
                    CreatedDate = x.CreateDate
                }).ToList();
            }
            catch (Exception ex)
            {

            }
            return PartialView("_RequestData", model);
        }
        public ActionResult Dashboard()
        {
            return View();
        }
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordVM model)
        {
            var Success = false;
            var Message = string.Empty;


            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                var userId = User.Identity.GetUserId();
                var result = await UserManager.ChangePasswordAsync(userId, model.OldPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await UserManager.FindByIdAsync(userId);
                    if (user != null)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    }
                    Success = true;
                    Message = "Password Changed Successfully";
                    //   return RedirectToAction("DashBoard", "DashBoard", new { area = "SuperAdmin" });
                }
                else
                {
                    Success = false;
                    if (result.Errors.Count() > 0)
                    {
                        foreach (var err in result.Errors)
                        {
                            if (err == "Incorrect password.")
                            {
                                Message = "Incorrect old password.";
                                break;
                            }
                            else
                            {
                                Message = err;
                                break;
                            }
                        }
                    }
                    //Message = "Password could not Match";
                }

            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return Json(new
            {
                Success = Success,
                Message = Message
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("LogIn", "Account", new { area = "" });

        }


    }
}