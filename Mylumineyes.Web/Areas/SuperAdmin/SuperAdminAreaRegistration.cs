﻿using System.Web.Mvc;

namespace Mylumineyes.Web.Areas.SuperAdmin
{
    public class SuperAdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "SuperAdmin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
         "admin/changepassword",
        "admin/changepassword",
         defaults: new { controller = "DashBoard", action = "ChangePassword", id = UrlParameter.Optional }
          );

            context.MapRoute(
         "admin/logoff",
        "admin/logoff",
         defaults: new { controller = "DashBoard", action = "LogOff", id = UrlParameter.Optional }
          );
            context.MapRoute(
                "admin/dashboard",
               "admin/dashboard",
                defaults: new { controller = "Dashboard", action = "Dashboard", id = UrlParameter.Optional }
                 );
            context.MapRoute(
                "SuperAdmin_default",
                "SuperAdmin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}